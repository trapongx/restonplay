# Users schema

# --- !Ups

CREATE TABLE Project (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Project_Members (
    project_id bigint(20) NOT NULL,
    employee_id bigint(20) NOT NULL,
    PRIMARY KEY (project_id, employee_id)
);

# --- !Downs

DROP TABLE Project;

DROP TABLE Project_Members;