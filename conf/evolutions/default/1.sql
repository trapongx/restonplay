# Users schema

# --- !Ups

CREATE TABLE Employee (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    firstname varchar(50) NOT NULL,
    lastname varchar(50) NOT NULL,
    department_id bigint(20) NOT NULL,
    PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE Employee;