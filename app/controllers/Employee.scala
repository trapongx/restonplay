package controllers

import models.Employee
import play.api.libs.json._
import play.api.mvc._
import services.EmployeeService
import services.provider.ModelServiceProviderRegistry

/**
 * Created by trapongx on 3/29/2015.
 */
object Employee extends Controller {
  lazy val employeeService : EmployeeService = ModelServiceProviderRegistry.get(classOf[Employee]).getService().asInstanceOf[EmployeeService]

  implicit val employeeWrites = new Writes[Employee] {
    def writes(employee: Employee) = Json.obj(
      "id" -> employee.id,
      "firstname" -> employee.firstname,
      "lastname" -> employee.lastname,
      "departmentId" -> employee.departmentId
    )
  }

  def listByDepartmentId(departmentId : Long) = Action {
    Ok(Json.toJson(employeeService.listByDepartmentId(departmentId)))
  }

  def getById(id : Long) = Action {
    val e = employeeService.getById(id)
    if (e != null)
      Ok(Json.toJson(e))
    else
      NotFound(Json.toJson("Resource not found"))
  }

}
