package controllers

import models.{Employee, Project}
import play.api.libs.json._
import play.api.mvc._
import services.ProjectService
import services.provider.ModelServiceProviderRegistry

/**
 * Created by trapongx on 3/29/2015.
 */
object Project extends Controller {
  lazy val projectService : ProjectService = ModelServiceProviderRegistry.get(classOf[Project]).getService().asInstanceOf[ProjectService]

  implicit val projectWrites = new Writes[Project] {
    def writes(project: Project) = Json.obj(
      "id" -> project.id,
      "name" -> project.name
    )
  }

  implicit val employeeWrites = new Writes[Employee] {
    def writes(employee: Employee) = Json.obj(
      "id" -> employee.id,
      "firstname" -> employee.firstname,
      "lastname" -> employee.lastname,
      "departmentId" -> employee.departmentId
    )
  }

  def listAllProjectMembers(projectId : Long) = Action {
    Ok(Json.toJson(projectService.listAllProjectMembers(projectId)))
  }

  def getById(id : Long) = Action {
    val p = projectService.getById(id)
    if (p != null)
      Ok(Json.toJson(p))
    else
      NotFound(Json.toJson("Resource not found"))
  }

}
