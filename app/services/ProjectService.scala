package services

import models.{Employee, Project}

/**
 * Created by trapongx on 3/29/2015.
 */
abstract class ProjectService extends ModelService[Project]{
  /**
   * List all employee who are member of the project specified by projectId
   * @param projectId
   * @return array of Employee object. If no member at all, empty array is returned.
   */
  def listAllProjectMembers(projectId : Long) : Array[Employee]
}
