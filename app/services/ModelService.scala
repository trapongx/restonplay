package services

import models.{Model, Employee}

/**
 * Created by trapongx on 3/29/2015.
 */
abstract class ModelService[T <: Model] {
  /**
   * Get the model object having the specified ID
   * @param id of the object
   * @return object of type T, or null if does not exist
   */
  def getById(id : Long) : T

  def insert(obj : T)
}
