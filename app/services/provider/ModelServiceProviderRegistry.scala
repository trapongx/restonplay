package services.provider

import java.util

import models.{Project, Model, Employee}
import services.db.{InDbProjectService, InDbEmployeeService}
import services.dummy.{DummyProjectService, DummyEmployeeService}

/**
 * Created by trapongx on 3/30/2015.
 * This is the singleton registry that everyone in this application will ask for the model service provider of specific model.
 */
object ModelServiceProviderRegistry {
  private val map = new util.HashMap[Class[_ <: Model], ModelServiceProvider[_ <: Model]]()

  /**
   * Return provider for a specific model class registered. If none registered, null is returned.
   * @param clazz
   * @return
   */
  def get(clazz : Class[_ <: Model]) : ModelServiceProvider[_ <: Model] = {
    map.get(clazz)
  }

  /**
   * Register a provider for the specific clazz.
   * In order to unregister for this model clazz, just pass null provider.
   * @param clazz
   * @param provider
   * @return nothing
   */
  def set(clazz : Class[_ <: Model], provider : ModelServiceProvider[_ <: Model]) : Unit = {
    if (provider == null)
      map.remove(clazz)
    else
      map.put(clazz, provider)
  }

  /**
   * TODO
   * This method hard-coded-ly register selected providers. It can be modified to register other provider.
   * Or for better, use dependency injection that use provider class specified in configuration files.
   */
  private def initialize() : Unit = {
    set(classOf[Employee], new DummyOrInDbModelServiceProvider[Employee](DummyEmployeeService, InDbEmployeeService))
    set(classOf[Project], new DummyOrInDbModelServiceProvider[Project](DummyProjectService, InDbProjectService))
  }

  initialize()
}
