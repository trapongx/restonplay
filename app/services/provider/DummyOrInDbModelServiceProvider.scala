package services.provider

import models.{Project, Employee, Model}
import services.ModelService
import services.db.{InDbProjectService, InDbModelServiceProvider}
import services.dummy.{DummyProjectService, DummyModelServiceProvider}

/**
 * Created by trapongx on 3/30/2015.
 */
class DummyOrInDbModelServiceProvider[T <: Model](dummy : ModelService[T], inDb : ModelService[T])  extends ModelServiceProvider[T] with DummyModelServiceProvider[T] with InDbModelServiceProvider[T]{
  private val dummyModelService = dummy
  private val inDbModelService = inDb

  override def getService(): ModelService[T] = {
    val svc = getDummyModelService()
    if (svc != null) svc else getInDbModelService()
  }

  override def getDummyModelService(): ModelService[T] = {
    if (DummyModelServiceProvider.isUseDummy) dummyModelService else null
  }

  override def getInDbModelService(): ModelService[T] = inDbModelService
}
