package services.provider

import models.Model
import services.ModelService

/**
 * Created by trapongx on 3/29/2015.
 */
trait ModelServiceProvider[T <: Model] {
  def getService() :  ModelService[T]
}
