package services.db

import models.Model
import services.ModelService
import services.provider.ModelServiceProvider

/**
  * Created by trapongx on 3/29/2015.
  */
trait InDbModelServiceProvider[T <: Model] extends ModelServiceProvider[T] {
   override def getService() :  ModelService[T] = {getInDbModelService()}

   def getInDbModelService() : ModelService[T]
 }
