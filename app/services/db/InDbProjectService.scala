package services.db

import models.{Project, Employee}
import play.api.db.DB
import play.api.Play.current
import services.{ProjectService, EmployeeService}

import scala.collection.mutable.ArrayBuffer

/**
 * Created by trapongx on 3/29/2015.
 */
object InDbProjectService extends ProjectService {

  /**
   * List all employee who are member of the project specified by projectId
   * @param projectId
   * @return array of Employee object. If no member at all, empty array is returned.
   */
  override def listAllProjectMembers(projectId : Long) : Array[Employee] = {
    val employees = new ArrayBuffer[Employee]
    DB.withConnection { conn =>
      val stmt = conn.prepareStatement(
        """SELECT e.id, e.firstname, e.lastname, e.department_id
          |FROM Employee_Members em
          |INNER JOIN Employee e ON em.employee_id = e.id
          |WHERE project_id = ?""".stripMargin)
      stmt.setLong(1, projectId)
      val rs = stmt.executeQuery()
      while (rs.next()) {
        val employee = new Employee() {
          id = rs.getLong(1)
          firstname = rs.getString(2)
          lastname = rs.getString(3)
          departmentId = rs.getLong(4)
        }
        employees.append(employee)
      }
    }
    employees.toArray
  }

  /**
   * Get the model object having the specified ID
   * @param id of the object
   * @return object of type Project, or null if does not exist
   */
  override def getById(id: Long): Project = {
    var project : Project = null
    DB.withConnection { conn =>
      val stmt = conn.prepareStatement("SELECT name FROM Project WHERE id = ?")
      stmt.setLong(1, id)
      val rs = stmt.executeQuery()
      if (rs.next()) {
        project = new Project() {
          name = rs.getString(1)
        }
        project.id = id
      }
    }
    project
  }

  override def insert(obj: Project): Unit = {
    DB.withConnection { conn =>
      val stmt = conn.prepareStatement("INSERT INTO Project(id, name) VALUES(?, ?)")
      stmt.setLong(1, obj.id)
      stmt.setString(2, obj.name)
      stmt.executeUpdate()
    }
  }

  def addMember(projectId : Long, employeeId : Long) : Unit = {
    DB.withConnection { conn =>
      val stmt = conn.prepareStatement("INSERT INTO Project_Members(project_id, employee_id) VALUES(?, ?)")
      stmt.setLong(1, projectId)
      stmt.setLong(2, employeeId)
      stmt.executeUpdate()
    }
  }

  private def initializeData(): Unit = {
    val projects = for (i <- 1 to 10) yield new Project() {id=i; name="Test Project DB " + i}
    for (p <- projects) insert(p)
    for (employeeId <- 1 to 3) addMember(1, employeeId)
    for (employeeId <- 4 to 5) addMember(2, employeeId)
  }

  initializeData()  
}
