package services.db

import models.Employee
import play.api.db.DB
import play.api.Play.current
import services.EmployeeService

import scala.collection.mutable.ArrayBuffer

/**
 * Created by trapongx on 3/29/2015.
 */
object InDbEmployeeService extends EmployeeService {

  /**
   * List all employee work for the department specified by departmentId
   * @param departmentId
   * @return array of Employee. If no Employee at all, empty array is returned.
   */
  override def listByDepartmentId(departmentId: Long): Array[Employee] = {
    val employees = new ArrayBuffer[Employee]
    DB.withConnection { conn =>
      val stmt = conn.prepareStatement("SELECT id, firstname, lastname FROM Employee WHERE department_id = ?")
      stmt.setLong(1, departmentId)
      val rs = stmt.executeQuery()
      while (rs.next()) {
        val employee = new Employee() {
          id = rs.getLong(1)
          firstname = rs.getString(2)
          lastname = rs.getString(3)
        }
        employee.departmentId = departmentId
        employees.append(employee)
      }
    }
    employees.toArray
  }

  /**
   * Get the model object having the specified ID
   * @param id of the object
   * @return object of type Employee, or null if does not exist
   */
  override def getById(id: Long): Employee = {
    var employee : Employee = null
    DB.withConnection { conn =>
      val stmt = conn.prepareStatement("SELECT firstname, lastname, department_id FROM Employee WHERE id = ?")
      stmt.setLong(1, id)
      val rs = stmt.executeQuery()
      if (rs.next()) {
        employee = new Employee() {
          firstname = rs.getString(1)
          lastname = rs.getString(2)
          departmentId = rs.getLong(3)
        }
        employee.id = id
      }
    }
    employee
  }

  override def insert(obj: Employee): Unit = {
    DB.withConnection { conn =>
      val stmt = conn.prepareStatement("INSERT INTO Employee(id, firstname, lastname, department_id) VALUES(?, ?, ?, ?)")
      stmt.setLong(1, obj.id)
      stmt.setString(2, obj.firstname)
      stmt.setString(3, obj.lastname)
      stmt.setLong(4, obj.departmentId)
      stmt.executeUpdate()
    }
  }

  private def initializeData(): Unit = {
    val employees = Array[Employee](
      new Employee(){id = 1; firstname = "Andre-db"; lastname = "Hanzellous"; departmentId = 1},
      new Employee(){id = 2; firstname = "Joshure-db"; lastname = "Nightington"; departmentId = 1},
      new Employee(){id = 3; firstname = "Eusope-db"; lastname = "Hinderguard"; departmentId = 2},
      new Employee(){id = 4; firstname = "Laksami-db"; lastname = "Srikanth"; departmentId = 2},
      new Employee(){id = 5; firstname = "Izzabella-db"; lastname = "Daindra"; departmentId = 1}
    )
    for (e <- employees) insert(e)
  }

  initializeData()

}
