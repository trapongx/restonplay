package services

import models.Employee

/**
 * Created by trapongx on 3/29/2015.
 */
abstract class EmployeeService extends ModelService[Employee]{
  /**
   * List all employee work for the department specified by departmentId
   * @param departmentId
   * @return array of Employee. If no Employee at all, empty array is returned.
   */
  def listByDepartmentId(departmentId : Long) : Array[Employee]
}
