package services.dummy

import models.Model
import services.ModelService
import services.provider.ModelServiceProvider

/**
 * Created by trapongx on 3/29/2015.
 */
trait DummyModelServiceProvider[T <: Model] extends ModelServiceProvider[T] {
  override def getService() :  ModelService[T] = {getDummyModelService()}

  def getDummyModelService() : ModelService[T]
}

object DummyModelServiceProvider {
  lazy val isUseDummy = {
    val s = System.getProperty("restOnPlay.useDummyModelService")
    if (s != null) s.equalsIgnoreCase("true") else false
  }
}
