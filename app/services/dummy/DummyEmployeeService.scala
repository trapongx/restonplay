package services.dummy

import models.Employee
import services.EmployeeService

/**
 * Created by trapongx on 3/29/2015.
 */
object DummyEmployeeService extends EmployeeService {

  private val employees = Array[Employee](
    new Employee(){id = 1; firstname = "Andre-dummy"; lastname = "Hanzellous"; departmentId = 1},
    new Employee(){id = 2; firstname = "Joshure-dummy"; lastname = "Nightington"; departmentId = 1},
    new Employee(){id = 3; firstname = "Eusope-dummy"; lastname = "Hinderguard"; departmentId = 2},
    new Employee(){id = 4; firstname = "Laksami-dummy"; lastname = "Srikanth"; departmentId = 2},
    new Employee(){id = 5; firstname = "Izzabella-dummy"; lastname = "Daindra"; departmentId = 1}
  )

  /**
   * List all employee work for the department specified by departmentId
   * @param departmentId
   * @return array of Employee. If no Employee at all, empty array is returned.
   */
  override def listByDepartmentId(departmentId: Long): Array[Employee] = {
    employees.filter(e => e.departmentId == departmentId)
  }

  /**
   * Get the model object having the specified ID
   * @param id of the object
   * @return object of type T, or null if does not exist
   */
  override def getById(id: Long): Employee = {
    employees.filter(e => e.id == id).head //TODO need optimization
  }

  override def insert(obj: Employee): Unit = ??? //Does not supported
}
