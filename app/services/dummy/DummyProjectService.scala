package services.dummy

import models.{Project, Employee}
import services.provider.ModelServiceProviderRegistry
import services.{EmployeeService, ProjectService}

import scala.collection.mutable.ArrayBuffer

/**
 * Created by trapongx on 3/29/2015.
 */
object DummyProjectService extends ProjectService {

  lazy val employeeService : EmployeeService = ModelServiceProviderRegistry.get(classOf[Employee]).getService().asInstanceOf[EmployeeService]

  private val projects = for (i <- 1 to 10) yield new Project() {id=i; name="Test Project Dummy " + i}

  private val projectMembersMap = Map[Long,Array[Long]](1L -> Array(1,2,3), 2L -> Array(4,5))

  /**
   * List all employee who are member of the project specified by projectId
   * @param projectId
   * @return array of Employee object. If no member at all, empty array is returned.
   */
  override def listAllProjectMembers(projectId: Long): Array[Employee] = {
    val membersIds = projectMembersMap(projectId)
    val employees: ArrayBuffer[Employee] = new ArrayBuffer[Employee]
    if (membersIds != null) {
      for (employeeId <- membersIds) {
        val e = employeeService.getById(employeeId)
        if (e != null)
          employees.append(e)
      }
    }
    employees.toArray
  }

  /**
   * Get the model object having the specified ID
   * @param id of the object
   * @return object of type T, or null if does not exist
   */
  override def getById(id: Long): Project = {
    projects.filter(e => e.id == id).head //TODO need optimization
  }

  override def insert(obj: Project): Unit = ??? //Does not supported
}
