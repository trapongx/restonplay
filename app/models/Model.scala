package models

/**
 * Created by trapongx on 3/29/2015.
 */
abstract class Model {
  /**
   * ID start from 1 for all Model.
   * Value of 0 indicate unassigned ID.
   * Mandatory for persistence.
   */
  var id: Long = 0
}
